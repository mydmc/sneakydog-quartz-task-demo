package dog.sneaky.demo.dao;

import dog.sneaky.demo.entity.Job;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;



@Mapper
public interface JobMapper {

    public List<Job> selectJobList(Job job);

    public List<Job> selectJobAll();

    public Job selectJobById(Long jobId);

    public int deleteJobById(Long jobId);

    public int deleteJobByIds(Long[] ids);

    public int updateJob(Job job);

    public int insertJob(Job job);
}
