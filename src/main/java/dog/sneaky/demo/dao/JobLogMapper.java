package dog.sneaky.demo.dao;

import dog.sneaky.demo.entity.JobLog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface JobLogMapper {

    List<JobLog> selectJobLogList(JobLog jobLog);

    List<JobLog> selectJobLogAll();

    JobLog selectJobLogById(Long jobLogId);

    int insertJobLog(JobLog jobLog);

    int deleteJobLogByIds(String[] ids);

    int deleteJobLogById(Long jobId);

    void cleanJobLog();
}
