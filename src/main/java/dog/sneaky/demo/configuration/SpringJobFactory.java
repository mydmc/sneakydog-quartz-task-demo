package dog.sneaky.demo.configuration;

import lombok.RequiredArgsConstructor;
import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.AdaptableJobFactory;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SpringJobFactory extends AdaptableJobFactory {
    private final AutowireCapableBeanFactory capableBeanFactory;
    private final ApplicationContext applicationContext;

    @Override
    protected Object createJobInstance(TriggerFiredBundle bundle) throws Exception {
        BaseJob jobInstance = (BaseJob) super.createJobInstance(bundle);
        jobInstance.setApplicationContext(applicationContext);
        capableBeanFactory.autowireBean(jobInstance);
        return jobInstance;
    }
}
