package dog.sneaky.demo.configuration;

import dog.sneaky.demo.service.IJobService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@RequiredArgsConstructor
public class SchedulerCommandLineRunner implements CommandLineRunner {
    private final IJobService iJobService;

    @Override
    public void run(String... args) throws Exception {
        iJobService.initAllJob();
    }
}
