package dog.sneaky.demo.configuration;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@RequiredArgsConstructor
public class JobAfterExecutionEventListener {

    @EventListener(JobAfterExecutionEvent.class)
    public void execute(JobAfterExecutionEvent event) {
        String context = event.getContext();
        System.out.println(context);
    }

}
