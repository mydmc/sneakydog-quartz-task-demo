package dog.sneaky.demo.configuration;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;

import java.io.Serializable;


@DisallowConcurrentExecution
public abstract class BaseJob implements Job, Serializable {
    protected ApplicationContext applicationContext;
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
            execute();
        } catch (Exception e) {
            e.printStackTrace();
            applicationContext.publishEvent(new JobAfterExecutionEvent(this, e.getMessage()));
        }
    }

    abstract public void execute() throws Exception;
}
