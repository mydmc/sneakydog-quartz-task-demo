package dog.sneaky.demo.configuration;

import org.springframework.context.ApplicationEvent;


public class JobAfterExecutionEvent extends ApplicationEvent {
    private final String context;

    public JobAfterExecutionEvent(Object source, String context) {
        super(source);
        this.context = context;
    }

    public String getContext() {
        return context;
    }
}
