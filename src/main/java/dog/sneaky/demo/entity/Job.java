package dog.sneaky.demo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class Job implements Serializable {

    private Long jobId;
    private String jobName;
    private String jobGroup;
    private String invokeTarget;
    private String cronExpression;
    //    @Excel(name = "计划策略 ", readConverterExp = "0=默认,1=立即触发执行,2=触发一次执行,3=不触发立即执行")
    private String misfirePolicy = "0";
    //    @Excel(name = "并发执行", readConverterExp = "0=允许,1=禁止")
    private String concurrent;
    //    @Excel(name = "任务状态", readConverterExp = "0=正常,1=暂停")
    private String status;

    private String createBy;

    private Date createTime;


    private String updateBy;


    private Date updateTime;

    private String remark;

    public boolean isPause() {
        return "1".equalsIgnoreCase(getStatus());
    }


}