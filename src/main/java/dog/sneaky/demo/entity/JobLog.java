package dog.sneaky.demo.entity;

import java.util.Date;


public class JobLog  {

    private Long jobLogId;

    private String jobName;

    private String jobGroup;

    private String invokeTarget;

    private String jobMessage;

//    @Excel(name = "执行状态", readConverterExp = "0=正常,1=失败")
    private String status;
    private String exceptionInfo;

    private Date startTime;
    private Date endTime;

}
