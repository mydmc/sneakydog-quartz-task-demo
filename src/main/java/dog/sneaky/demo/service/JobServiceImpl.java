package dog.sneaky.demo.service;


import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import dog.sneaky.demo.configuration.BaseJob;
import dog.sneaky.demo.dao.JobMapper;
import dog.sneaky.demo.entity.Job;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.reflections.Reflections;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
class JobServiceImpl implements IJobService{
    private final Scheduler scheduler;
    private final JobMapper jobMapper;


    @Override
    public Job queryById(Long id) {
        return jobMapper.selectJobById(id);
    }


    @Override
    public void initAllJob() throws Exception{
        List<Job> jobList = jobMapper.selectJobAll();
        for (Job job : jobList) {
            createScheduleJob(job);
        }
    }

    @Override
    public void save(Job job) {
        Long jobId = job.getJobId();
        Job  dbJob = jobMapper.selectJobById(jobId);
        if (dbJob != null) {
            dbJob.setJobName(job.getJobName());
            dbJob.setCronExpression(job.getCronExpression());
            dbJob.setRemark(job.getRemark());
            dbJob.setInvokeTarget(job.getInvokeTarget());
            dbJob.setUpdateTime(new Date());
            jobMapper.updateJob(dbJob);
        } else {
            dbJob = new Job();
            dbJob.setJobGroup("DEFAULT");
            dbJob.setJobName(job.getJobName());
            dbJob.setCronExpression(job.getCronExpression());
            dbJob.setRemark(job.getRemark());
            dbJob.setInvokeTarget(job.getInvokeTarget());
            dbJob.setStatus("1");
            dbJob.setCreateTime(new Date());
            jobMapper.insertJob(dbJob);
        }
    }

    @Override
    public Object queryPage(int pageNumber, int pageSize, String jobName) {

        Job queryJob = new Job();
        queryJob.setJobName(jobName);

        Page<Job> jobPage = PageHelper.startPage(pageNumber, pageSize).doSelectPage(new ISelect() {
            @Override
            public void doSelect() {
                jobMapper.selectJobList(queryJob);
            }
        });

        return new HashMap<String, Object>() {{
            put("pageCount", jobPage.getPages());
            put("itemCount", jobPage.getTotal());
            put("data", jobPage.getResult());
        }};
    }

    @Override
    public Object queryByJobName(int pageNumber, int pageSize, String jobName) {

        Job queryJob = new Job();
        queryJob.setJobName(jobName);

        Page<Job> jobPage = PageHelper.startPage(pageNumber, pageSize).doSelectPage(new ISelect() {
            @Override
            public void doSelect() {
                jobMapper.selectJobList(queryJob);
            }
        });

        return new HashMap<String, Object>() {{
            put("total", jobPage.getTotal());
            put("rows", jobPage.getResult());
        }};
    }

    private void createScheduleJob(Job job) throws Exception{
        Long jobId = job.getJobId();
        String jobGroup = job.getJobGroup();

        Class<? extends org.quartz.Job> jobClass = findJobClass(job.getInvokeTarget());

        if (jobClass != null) {
            jobClass.newInstance();

            JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(getJobKey(jobId, jobGroup)).build();

            CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(job.getCronExpression());
            CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(getTriggerKey(jobId, jobGroup)).withSchedule(cronScheduleBuilder).build();

            jobDetail.getJobDataMap().put("QRTZ_JOB_CLASS", job);

            JobKey jobKey = jobDetail.getKey();
            if (scheduler.checkExists(jobKey)) {
                scheduler.deleteJob(jobKey);
            }

            scheduler.scheduleJob(jobDetail, trigger);
            if (job.isPause()) {
                scheduler.pauseJob(jobKey);
            }
        }

    }

    public TriggerKey getTriggerKey(Long jobId, String jobGroup) {
        return TriggerKey.triggerKey("QRTZ_" + jobId, jobGroup);
    }

    public JobKey getJobKey(Long jobId, String jobGroup) {
        return JobKey.jobKey("QRTZ_" + jobId, jobGroup);
    }


    private Class<? extends org.quartz.Job> findJobClass(String jobName){
        Reflections reflections = new Reflections("dog.sneaky.demo.job");
        Set<Class<? extends BaseJob>> subTypesOfBaseJob = reflections.getSubTypesOf(BaseJob.class);

        Class<? extends org.quartz.Job> jobClass = null;
        if (subTypesOfBaseJob != null && !subTypesOfBaseJob.isEmpty()) {
            for (Class<? extends BaseJob> aClass : subTypesOfBaseJob) {
                if (jobName.equalsIgnoreCase(aClass.getSimpleName())) {
                    jobClass  = aClass;
                    break;
                }
            }
        }


        return jobClass;
    }

}
