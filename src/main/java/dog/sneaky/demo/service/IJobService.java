package dog.sneaky.demo.service;

import dog.sneaky.demo.entity.Job;

public interface IJobService {
    Job queryById(Long id);
    Object queryByJobName(int pageNumber, int pageSize, String jobName);
    void initAllJob() throws Exception;
    void save(Job job);

    Object queryPage(int pageNumber, int pageSize, String jobName);
}
