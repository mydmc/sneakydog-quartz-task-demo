package dog.sneaky.demo.controller;


import dog.sneaky.demo.entity.Job;
import dog.sneaky.demo.service.IJobService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Slf4j
@Controller
@RequestMapping("/job")
@RequiredArgsConstructor
public class JobController {
    private final IJobService iJobService;

    @GetMapping("/add")
    public String add(Model model){
        model.addAttribute("job", new Job());
        return "add";
    }

    @GetMapping("/{jobId}/edit")
    public String edit(@PathVariable("jobId") Long jobId, Model model){
        model.addAttribute("job", iJobService.queryById(jobId));
        return "add";
    }


    @PostMapping("/save")
    public String save(Job job){
        iJobService.save(job);
        return "redirect:/job/list";
    }


    @GetMapping("/list")
    public String list(){
        System.out.println("123131");
        return "list";
    }

}
