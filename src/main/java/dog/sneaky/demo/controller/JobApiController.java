package dog.sneaky.demo.controller;


import dog.sneaky.demo.service.IJobService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/job")
@RequiredArgsConstructor
public class JobApiController {
    private final IJobService iJobService;

    @GetMapping("/list")
    public Object query(
            @RequestParam("pageNumber") int pageNumber,
            @RequestParam("pageSize") int pageSize,
            @RequestParam("jobName") String jobName){
        return iJobService.queryByJobName(pageNumber, pageSize, jobName);
    }


    @GetMapping("/query")
    public Object queryjob(
            @RequestParam("pageNumber") int pageNumber,
            @RequestParam("pageSize") int pageSize,
            @RequestParam("jobName") String jobName){
        return iJobService.queryPage(pageNumber, pageSize, jobName);
    }

}
