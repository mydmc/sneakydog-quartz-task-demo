package dog.sneaky.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class DefaultController {

    @GetMapping({"", "/", "/index", "index.html"})
    public String index(){
        return "redirect:/job/list";
    }
}
